﻿using UnityEngine;

public class PlatformSelection : MonoBehaviour
{

	public bool usedForWeb;
	public bool usedForDesktop;
	public bool usedForMobile;
	public bool usedForOthers;

	// Use this for initialization
	void Start()
	{
		gameObject.SetActive(true);
		if (usedForWeb || usedForDesktop || usedForMobile || usedForOthers)
		{
			if (Application.isMobilePlatform)
			{
				gameObject.SetActive(usedForMobile);
			}
			else if (Application.isWebPlayer)
			{
				gameObject.SetActive(usedForWeb);
			}
			else if (Application.platform == RuntimePlatform.WindowsPlayer
				  || Application.platform == RuntimePlatform.WindowsEditor
				  || Application.platform == RuntimePlatform.LinuxPlayer
				  || Application.platform == RuntimePlatform.LinuxEditor
				  || Application.platform == RuntimePlatform.OSXPlayer
				  || Application.platform == RuntimePlatform.OSXEditor)
			{
				gameObject.SetActive(usedForDesktop);
			}
			else
			{
				gameObject.SetActive(usedForOthers);
			}
		}
		else
		{
			gameObject.SetActive(false);
		}
	}
}
