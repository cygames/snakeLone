﻿using System.Collections.Generic;
using UnityEngine;

public class Chain : MonoBehaviour {
//	public Chain previousObject = null;
	public Chain nextObject = null;
//	public Chain PreviousObjectPriority { get; set; }
	public Stack<Chain> previousObjectStack = new Stack<Chain>();
	private Vector3 offset = Vector3.zero;

	internal static Transform GetPrevious(Transform curObject)
	{
		return GetPrevious(curObject.GetComponent<Chain>());
	}

	internal static void PushPrevious(Transform curObject, Transform objectToPush)
	{
		PushPrevious(curObject.GetComponent<Chain>(), objectToPush.GetComponent<Chain>());
	}

	internal static Transform PopPrevious(Transform curObject)
	{
		return PopPrevious(curObject.GetComponent<Chain>());
	}

	internal static Transform GetPrevious(Chain curObject)
	{
		return curObject.previousObjectStack.Peek().transform;
	}

	internal static void PushPrevious(Chain curObject, Chain objectToPush)
	{
		if (curObject.previousObjectStack.Count > 0
			&& curObject.previousObjectStack.Peek().Equals(objectToPush))
		{
			curObject.previousObjectStack.Pop();
		}
		else
		{
			curObject.previousObjectStack.Push(objectToPush);
		}
	}

	internal static Transform PopPrevious(Chain curObject)
	{
		return curObject.previousObjectStack.Pop().transform;
	}

	internal static Vector3 GetOffset(Transform curObject)
	{
		return GetOffset(curObject.GetComponent<Chain>());
	}

	internal static Vector3 GetOffset(Chain curObject)
	{
		return curObject.offset;
	}

	internal static Vector3 AddOffset(Transform curObject, Vector3 offset)
	{
		return AddOffset(curObject.GetComponent<Chain>(), offset);
	}

	internal static Vector3 AddOffset(Chain curObject, Vector3 offset)
	{
		return curObject.offset += offset;
	}

	internal static Vector3 DeduceOffset(Transform curObject, Vector3 offset)
	{
		return AddOffset(curObject.GetComponent<Chain>(), -offset);
	}

	internal static Vector3 DeduceOffset(Chain curObject, Vector3 offset)
	{
		return AddOffset(curObject, -offset);
	}

}
