﻿using UnityEngine;

/*
 * Optimized version of ghosts used for fluid screen wrapping.
 * In essense only 1 is needed for either HORIZONTAL or VERTICAL and only 3 for BOTH.
 * 
 * HORIZONTAL/VERTICAL: Based on which half of the screen the main object is, have a ghost on the other side
 * BOTH: The 3 ghosts are:
 *		1 for HORIZONTAL following the logic of the only HORIZONTAL
 *		1 for VERTICAL   following the logic of the only VERTICAL
 *		1 for CORNER     similar to the others but based on quarters of screeen
 *		  e.g. if main object is in top-right corner then we only care about the ghost in the bottom-left corner
 */
public class Ghosts : MonoBehaviour {
	public enum WrappingMethod {NONE, HORIZONTAL, VERTICAL, BOTH};

	public WrappingMethod wrappingMethod = WrappingMethod.NONE;
	public bool wrapAround;
	public Snake objectToNotify;

    private Transform[] ghosts;
    private float screenWidth;
    private float screenHeight;
	private Chain chain;

	// Helper constants
	private enum Quadrant { TOP_LEFT = 0, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT };
	private const int RIGHT_MASK = 1;
	private const int BOTTOM_MASK = 2;

	// Use this for initialization
	void Start ()
    {
        int numGhosts = CalculateGhostsNeeded();

        // Instantiate needed ghosts or disable the component if not needed
        if (numGhosts != 0)
        {
            MeasureScreen();
			chain = GetComponent<Chain>();

            ghosts = new Transform[numGhosts];
            InstantiateGhosts();
            PositionGhosts();
        }
        else
        {
            enabled = false;
        }
    }

    // Calculate number of ghosts needed for wrapping
	private int CalculateGhostsNeeded()
	{
		int numGhosts = 0;

		switch (wrappingMethod)
		{
			case WrappingMethod.HORIZONTAL:
				numGhosts = 1;
				break;
			case WrappingMethod.VERTICAL:
				numGhosts = 1;
				break;
			case WrappingMethod.BOTH:
				numGhosts = 3;
				break;
			case WrappingMethod.NONE:
				numGhosts = 0;
				break;
		}

		return numGhosts;
	}

	private void MeasureScreen()
    {
        var cam = Camera.main;

        var screenBottomLeft = cam.ViewportToWorldPoint(new Vector3(0, 0, transform.position.z));
        var screenTopRight = cam.ViewportToWorldPoint(new Vector3(1, 1, transform.position.z));

        screenWidth = screenTopRight.x - screenBottomLeft.x;
        screenHeight = screenTopRight.y - screenBottomLeft.y;
    }

	private void InstantiateGhosts()
    {
        for (int i = 0; i < ghosts.Length; i++)
        {
			// Use the main gameObject but remove the Ghosts script
			ghosts[i] = Instantiate(gameObject.transform);
			Destroy(ghosts[i].GetComponent<Ghosts>());
		}
    }

	private void PositionGhosts()
    {
		// Identify quadrant main object is in
		// 0(00b): TOP-LEFT
		// 1(01b): TOP-RIGHT
		// 2(10b): BOTTOM-LEFT
		// 3(11b): BOTTOM-RIGHT
		int mainObjectQuadrant = (transform.position.x < 0 ? 0 : RIGHT_MASK) + (transform.position.y < 0 ? 0 : BOTTOM_MASK);

        switch (wrappingMethod)
        {
            case WrappingMethod.HORIZONTAL:
				if ((mainObjectQuadrant & RIGHT_MASK) > 0)
				{
					PositionGhost(0, -screenWidth, 0f);     // LEFT
				}
				else
				{
					PositionGhost(0, screenWidth, 0f);      // RIGHT
				}
                break;
            case WrappingMethod.VERTICAL:
				if ((mainObjectQuadrant & BOTTOM_MASK) > 0)
				{
					PositionGhost(0, 0f, -screenHeight);    // TOP
				}
				else
				{
					PositionGhost(0, 0f, screenHeight);     // BOTTOM
				}
                break;
            case WrappingMethod.BOTH:
				if (mainObjectQuadrant == (int)Quadrant.TOP_LEFT)
				{
					PositionGhost(0, screenWidth, 0f);      // RIGHT
					PositionGhost(1, 0f, screenHeight);     // BOTTOM
					PositionGhost(2, screenWidth, screenHeight);    // BOTTOM-RIGHT
				}
				else if (mainObjectQuadrant == (int)Quadrant.TOP_RIGHT)
				{
					PositionGhost(0, -screenWidth, 0f);     // LEFT
					PositionGhost(1, 0f, screenHeight);     // BOTTOM
					PositionGhost(2, -screenWidth, screenHeight);   // BOTTOM-LEFT
				}
				else if (mainObjectQuadrant == (int)Quadrant.BOTTOM_LEFT)
				{
					PositionGhost(0, screenWidth, 0f);      // RIGHT
					PositionGhost(1, 0f, -screenHeight);    // TOP
					PositionGhost(2, screenWidth, -screenHeight);   // TOP-RIGHT
				}
				else // mainObjectQuadrant == (int)Quadrant.BOTTOM_RIGHT
				{
					PositionGhost(0, -screenWidth, 0f);     // LEFT
					PositionGhost(1, 0f, -screenHeight);    // TOP
					PositionGhost(2, -screenWidth, -screenHeight);  // TOP-LEFT
				}
                break;
        }

        foreach (var ghost in ghosts)
        {
            ghost.rotation = transform.rotation;
        }
    }

	/**
	 * Set position of the index'th ghost relative to the main object
	 */
    private void PositionGhost(int index, float xOffset, float yOffset)
    {
        var ghostPosition = transform.position;
        ghostPosition.x = transform.position.x + xOffset;
        ghostPosition.y = transform.position.y + yOffset;
        ghosts[index].position = ghostPosition;
    }

    // Update is called once per frame
    void Update ()
    {
        PositionGhosts();
    }

	// TODO Create a deligate for this method and register Snake on it
	/**
	 * Wrap main object once it leaves the screen
	 */
	void OnBecameInvisible()
	{
		if (wrapAround)
		{
			int ghostToFollow = ChooseGhostToFollow();

			// TODO Normalize distance to use +/- screenWidth/screenHeight to remove artifacts
			Vector3 distance = transform.position - ghosts[ghostToFollow].position;
			SwapWithGhost(ghostToFollow);

			if (!name.Equals("SnakeHead"))
			{
				Chain.DeduceOffset(GetComponent<Chain>(), distance);
			}

			if (chain.nextObject != null)
			{
				Chain.AddOffset(chain.nextObject, distance);
			}
		}
	}

	public void SwapWithGhost(int ghostToFollow)
	{
		Debug.Log("Wrapping " + gameObject.name + "(" + transform.position + ") using " + ghosts[ghostToFollow].name + "(" + ghosts[ghostToFollow].position + ")");
		transform.position = ghosts[ghostToFollow].position;
	}

	private int ChooseGhostToFollow()
	{
		int ghostToFollow;

		/*
		 * Calculate position of ghost in the following grid using the index {horizontalAlignment}{verticalAlignment}
		 * 
		 * 02 | 12 | 22
		 * ------------
		 * 01 | 11 | 21
		 * ------------
		 * 00 | 10 | 20
		 */
		int horizontalAlignment = 1;
		int verticalAlignment = 1;

		if (transform.position.x < -screenWidth / 2)
		{
			horizontalAlignment = 0;
		}
		else if (transform.position.x > screenWidth / 2)
		{
			horizontalAlignment = 2;
		}

		if (transform.position.y < -screenHeight / 2)
		{
			verticalAlignment = 0;
		}
		else if (transform.position.y > screenHeight / 2)
		{
			verticalAlignment = 2;
		}

		if (horizontalAlignment != 1)
		{
			if (verticalAlignment == 1)
			{
				ghostToFollow = 0;
			}
			else
			{
				ghostToFollow = 2;
			}
		}
		else
		{
			ghostToFollow = 1;
		}

		return ghostToFollow;
	}
}
