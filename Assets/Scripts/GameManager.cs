﻿using UnityEngine.UI;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public GameObject foodPrefab;
	public Snake snake;
	public GameObject gameCanvas;
	public Text scoreText;
	public GameObject gameOverCanvas;
	public Text gameOverScoreText;
	public AudioSource audioSource;
	public AudioClip eatSound;

	private int scoreValue = 0;
	private float foodBuffer;

	// Screen measurements
	private float effectiveMinScreenX;
	private float effectiveMaxScreenX;
	private float effectiveMinScreenY;
	private float effectiveMaxScreenY;
	private float screenWidth;
	private float screenHeight;

	// Use this for initialization
	void Start () {

		// Calculate screen size
		MeasureScreen();

		// Calculate buffer needed for the food prefab
		foodBuffer = Mathf.Max(foodPrefab.transform.localScale.x, foodPrefab.transform.localScale.y, foodPrefab.transform.localScale.z);

		// Calculate effective screen size for spawning food
		effectiveMinScreenX = (-screenWidth) / 2 + foodBuffer;
		effectiveMaxScreenX = (screenWidth) / 2 - foodBuffer;
		effectiveMinScreenY = (-screenHeight) / 2 + foodBuffer;
		effectiveMaxScreenY = (screenHeight) / 2 - foodBuffer;

		// Initialize score
		scoreText.text = "Score: " + scoreValue;

		// Spawn first food
		SpawnFood();
	}

	public void EatFood ()
	{
		audioSource.PlayOneShot(eatSound, 2f);
		// Increment score
		scoreText.text = "Score: " + (++scoreValue);

		// Increase snake body size
		snake.AddBodyPart();

		// Spawn new food
		SpawnFood();
	}

	public void BitSelf ()
	{
		// Stop snake movement
		snake.enabled = false;

		// Display game-over screen
		gameOverScoreText.text = "Final Score: " + scoreValue;
		gameCanvas.SetActive(false);
		gameOverCanvas.SetActive(true);
		audioSource.Stop();
	}

	private void MeasureScreen()
	{
		var cam = Camera.main;

		var screenBottomLeft = cam.ViewportToWorldPoint(new Vector3(0, 0, transform.position.z));
		var screenTopRight = cam.ViewportToWorldPoint(new Vector3(1, 1, transform.position.z));

		screenWidth = screenTopRight.x - screenBottomLeft.x;
		screenHeight = screenTopRight.y - screenBottomLeft.y;
	}

	private void SpawnFood()
	{
		float randX = Random.Range(effectiveMinScreenX, effectiveMaxScreenX);
		float randY = Random.Range(effectiveMinScreenY, effectiveMaxScreenY);
		Instantiate(foodPrefab, new Vector3(randX, randY, -0.5f), new Quaternion());
	}
}
