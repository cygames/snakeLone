﻿using UnityEngine;

public class Food : MonoBehaviour {

	private GameManager gameManager;

	private void Start()
	{
		gameManager = FindObjectOfType<GameManager>();
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.name.Equals("SnakeHead"))
		{
			gameManager.EatFood();

			Destroy(gameObject);
		}
	}
}
