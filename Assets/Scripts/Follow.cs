﻿using UnityEngine;

public class Follow : MonoBehaviour {

	public GameObject target;
	
	// Update is called once per frame
	void Update () {
		if (target)
		{
			Vector3 newPos = target.transform.position;
			newPos.z -= 10;
			transform.position = newPos;
		}
	}
}
