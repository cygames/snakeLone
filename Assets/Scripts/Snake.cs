﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Snake : MonoBehaviour {

    public GameObject snakeBody;
    public GameObject snakeHead;

	public Button counterClockwiseTurnButton;
	public Button clockwiseTurnButton;

	public int beginBodySize = 2;

	public float minDistance = 0.25f;
	public float speed = 1;
	public float rotationSpeed = 50;
	public GameObject bodyPrefab;

	public List<Transform> bodyParts = new List<Transform>();

	// Use this for initialization
	void Start () {
		Chain.PushPrevious(bodyParts[0], snakeHead.transform);
		while (bodyParts.Count < beginBodySize)
		{
			AddBodyPart();
		}
	}
	
	// Update is called once per frame
	void Update () {
        MoveSnakeHead();
	}

	private void LateUpdate()
	{
		MoveBodyParts();
	}

	public void AddBodyPart()
	{
		Vector3 newPos = bodyParts[bodyParts.Count - 1].position;
		Quaternion newRot = bodyParts[bodyParts.Count - 1].rotation;

		Transform newPart = (Instantiate(bodyPrefab, newPos, newRot) as GameObject).transform;
		newPart.SetParent(snakeBody.transform);

		// Connect the chain
		Chain newChain = newPart.GetComponent<Chain>();
		Chain oldChain = bodyParts[bodyParts.Count - 1].GetComponent<Chain>();
		Chain.PushPrevious(newChain, oldChain);
		oldChain.nextObject = newChain;

		bodyParts.Add(newPart);
	}

	private void MoveSnakeHead()
	{
		snakeHead.transform.Translate(GameController.instance.GetTranslation() * Time.smoothDeltaTime, Space.Self);

		snakeHead.transform.Rotate(GameController.instance.GetRotation() * Time.deltaTime);
	}

	private void MoveBodyParts()
	{
		for (int i = 0; i < bodyParts.Count; i++)
		{
			MoveBodyPart(bodyParts[i]);
		}
	}

	private void MoveBodyPart(Transform curBodyPart)
	{
		Transform prevBodyPart = Chain.GetPrevious(curBodyPart);
		Vector3 targetPosition = prevBodyPart.position + Chain.GetOffset(curBodyPart);
		Quaternion prevRotation = prevBodyPart.rotation;

		var distance = Vector3.Distance(targetPosition, curBodyPart.position);

		Vector3 newPos = targetPosition;

		newPos.z = snakeHead.transform.position.z;

		float T = Time.deltaTime * distance / minDistance * GameController.instance.GetMovementSpeed();

		if (T > 0.5f)
		{
			T = 0.5f;
		}

		curBodyPart.position = Vector3.Lerp(curBodyPart.position, newPos, T);
		curBodyPart.rotation = Quaternion.Lerp(curBodyPart.rotation, prevRotation, T);
	}
}
