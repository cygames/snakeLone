﻿using UnityEngine;

public class GameController : MonoBehaviour {

	public static GameController instance;
	public ButtonState counterClockwiseTurnButton;
	public ButtonState clockwiseTurnButton;

	public float movementSpeed = 1f;
	public float rotationSpeed = 50f;

	private GameController()
	{

	}

	private void Start()
	{
		instance = GetComponent<GameController>();
	}

	public float GetMovementSpeed()
	{
		return movementSpeed;
	}

	public Vector3 GetTranslation()
	{
		float modifier = 1f;

		if (Application.isMobilePlatform)
		{
			if (counterClockwiseTurnButton.IsPressed()
			&& clockwiseTurnButton.IsPressed())
			{
				modifier = 2f;
			}
		}
		else
		{
			if (Input.GetKey(KeyCode.Space))
			{
				modifier = 2f;
			}
		}

		return Vector3.up * movementSpeed * modifier;
	}

	public Vector3 GetRotation()
	{
		float modifier = 0f;

		if (Application.isMobilePlatform)
		{
			if (counterClockwiseTurnButton.IsPressed())
			{
				modifier -= 1f;
			}

			if (clockwiseTurnButton.IsPressed())
			{
				modifier += 1f;
			}
		}
		else
		{
			modifier = Input.GetAxis("Horizontal");
		}

		return Vector3.back * rotationSpeed * modifier;
	}
}
