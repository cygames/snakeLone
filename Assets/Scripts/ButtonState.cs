﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonState : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
	private bool isPressed;

	public void OnPointerDown(PointerEventData eventData)
	{
		isPressed = true;
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		isPressed = false;
	}

	public bool IsPressed()
	{
		return isPressed;
	}
}
